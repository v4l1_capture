/*
    Copyright (C) 2008 Brian Johnson

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#include <stdlib.h>
#include <string.h>
#include <syslog.h>
#include <popt.h>
#include <signal.h>
#include <fcntl.h>

#include <sys/mman.h>
#include <sys/ioctl.h>
#include <sys/errno.h>

#include <fusd.h>

#include <linux/videodev.h>
#include <linux/videodev2.h>

#include "v4l1_capture.h"

char * v4l2_dev = "/dev/video0";
char * v4l1_dev = "/dev/video1";

struct fusd_file_operations v4l1_file_ops =
{
	open:		v4l1_open,
	close:		v4l1_close,
	read:		v4l1_read,
	ioctl:		v4l1_ioctl,
	mmap:		v4l1_mmap,
};

int get_v4l_control(int fd, int cid)
{
	struct v4l2_queryctrl	v4l2qctrl;
	struct v4l2_control	v4l2ctrl;
	int			ret;

	v4l2qctrl.id = cid;
	ret = ioctl(fd, VIDIOC_QUERYCTRL, &v4l2qctrl);
	if (ret == 0 &&
	    !(v4l2qctrl.flags & V4L2_CTRL_FLAG_DISABLED)) {
		v4l2ctrl.id = v4l2qctrl.id;
		ret = ioctl(fd, VIDIOC_G_CTRL, &v4l2ctrl);
		if (ret < 0) {
			return 0;
		}
		return ((v4l2ctrl.value - v4l2qctrl.minimum) * 65535
			+ (v4l2qctrl.maximum - v4l2qctrl.minimum) / 2)
			/ (v4l2qctrl.maximum - v4l2qctrl.minimum);
	}
	return 0;
}

int set_v4l_control(int fd, int cid, int value)
{
	struct v4l2_queryctrl v4l2qctrl;
	struct v4l2_control v4l2ctrl;
	int ret;

	v4l2qctrl.id = cid;
	ret = ioctl(fd, VIDIOC_QUERYCTRL, &v4l2qctrl);
	if (ret == 0 &&
	    !(v4l2qctrl.flags & V4L2_CTRL_FLAG_DISABLED) &&
	    !(v4l2qctrl.flags & V4L2_CTRL_FLAG_GRABBED)) {
		if (value < 0)
			value = 0;
		if (value > 65535)
			value = 65535;
		if (value && v4l2qctrl.type == V4L2_CTRL_TYPE_BOOLEAN)
			value = 65535;
		v4l2ctrl.id = v4l2qctrl.id;
		v4l2ctrl.value = (value *
			(v4l2qctrl.maximum - v4l2qctrl.minimum) + 32767)
			/ 65535;
		v4l2ctrl.value += v4l2qctrl.minimum;
		ret = ioctl(fd, VIDIOC_S_CTRL, &v4l2ctrl);
	}
	return 0;
}

const static unsigned int palette2pixelformat[] = {
	[VIDEO_PALETTE_GREY]    = V4L2_PIX_FMT_GREY,
	[VIDEO_PALETTE_RGB555]  = V4L2_PIX_FMT_RGB555,
	[VIDEO_PALETTE_RGB565]  = V4L2_PIX_FMT_RGB565,
	[VIDEO_PALETTE_RGB24]   = V4L2_PIX_FMT_BGR24,
	[VIDEO_PALETTE_RGB32]   = V4L2_PIX_FMT_BGR32,
	/* yuv packed pixel */
	[VIDEO_PALETTE_YUYV]    = V4L2_PIX_FMT_YUYV,
	[VIDEO_PALETTE_YUV422]  = V4L2_PIX_FMT_YUYV,
	[VIDEO_PALETTE_UYVY]    = V4L2_PIX_FMT_UYVY,
	/* yuv planar */
	[VIDEO_PALETTE_YUV410P] = V4L2_PIX_FMT_YUV410,
	[VIDEO_PALETTE_YUV420]  = V4L2_PIX_FMT_YUV420,
	[VIDEO_PALETTE_YUV420P] = V4L2_PIX_FMT_YUV420,
	[VIDEO_PALETTE_YUV411P] = V4L2_PIX_FMT_YUV411P,
	[VIDEO_PALETTE_YUV422P] = V4L2_PIX_FMT_YUV422P,
};

unsigned int palette_to_pixelformat(unsigned int palette)
{
	if (palette < ARRAY_SIZE(palette2pixelformat))
		return palette2pixelformat[palette];
	else
		return 0;
}

unsigned int pixelformat_to_palette(unsigned int pixelformat)
{
	int palette = 0;
	switch (pixelformat)
	{
	case V4L2_PIX_FMT_GREY:
		palette = VIDEO_PALETTE_GREY;
		break;
	case V4L2_PIX_FMT_RGB555:
		palette = VIDEO_PALETTE_RGB555;
		break;
	case V4L2_PIX_FMT_RGB565:
		palette = VIDEO_PALETTE_RGB565;
		break;
	case V4L2_PIX_FMT_BGR24:
		palette = VIDEO_PALETTE_RGB24;
		break;
	case V4L2_PIX_FMT_BGR32:
		palette = VIDEO_PALETTE_RGB32;
		break;
	/* yuv packed pixel */
	case V4L2_PIX_FMT_YUYV:
		palette = VIDEO_PALETTE_YUYV;
		break;
	case V4L2_PIX_FMT_UYVY:
		palette = VIDEO_PALETTE_UYVY;
		break;
	/* yuv planar */
	case V4L2_PIX_FMT_YUV410:
		palette = VIDEO_PALETTE_YUV410P;
		break;
	case V4L2_PIX_FMT_YUV420:
		palette = VIDEO_PALETTE_YUV420P;
		break;
	case V4L2_PIX_FMT_YUV411P:
		palette = VIDEO_PALETTE_YUV411P;
		break;
	case V4L2_PIX_FMT_YUV422P:
		palette = VIDEO_PALETTE_YUV422P;
		break;
	}
	return palette;
}

int count_inputs(int fd)
{
	struct v4l2_input v4l2input;
	int i;
	for (i = 0;; i++) {
		memset(&v4l2input, 0, sizeof(v4l2input));
		v4l2input.index = i;
		if (0 != ioctl(fd, VIDIOC_ENUMINPUT, &v4l2input))
			break;
	}
	return i;
}

int check_size(int fd, int *width, int *height)
{
	struct v4l2_fmtdesc v4l2desc;
	struct v4l2_format  v4l2fmt;

	memset(&v4l2desc, 0, sizeof(v4l2desc));
	memset(&v4l2fmt, 0, sizeof(v4l2fmt));

	v4l2desc.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	if (0 != ioctl(fd, VIDIOC_ENUM_FMT, &v4l2desc))
		goto done;

	v4l2fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	v4l2fmt.fmt.pix.width       = *width;
	v4l2fmt.fmt.pix.height      = *height;
	v4l2fmt.fmt.pix.pixelformat = v4l2desc.pixelformat;
	if (0 != ioctl(fd, VIDIOC_TRY_FMT, &v4l2fmt))
		goto done;

	*width = v4l2fmt.fmt.pix.width;
	*height = v4l2fmt.fmt.pix.height;

 done:
	return 0;
}

int v4l1_open(struct fusd_file_info *file)
{
	struct v4l1_device *dev = (struct v4l1_device *)file->device_info;
	int ret = 0;
	dev->v4l2_fd = v4l2_open(v4l2_dev);
	if (dev->v4l2_fd < 0) {
		return -EINVAL;
	}
	ret = v4l1_alloc_buffer(&dev->v4l1buffers, NUM_BUFFERS);
	if (ret < 0) {
		close(dev->v4l2_fd);
		return ret;
	}
	ret = v4l2_request_buffers(dev, NUM_BUFFERS);
	if (ret < 0) {
		v4l1_free_buffer(&dev->v4l1buffers);
		close(dev->v4l2_fd);
		return ret;
	}
	v4l2_stream_on(dev);
	return ret;
}

int v4l1_close(struct fusd_file_info *file)
{
	struct v4l1_device *dev = (struct v4l1_device *)file->device_info;
	v4l2_stream_off(dev);
	v4l1_free_buffer(&dev->v4l1buffers);
	v4l2_free_buffers(dev);
	close(dev->v4l2_fd);
	return 0;
}

int v4l1_ioctl(struct fusd_file_info *file, int cmd, void *arg)
{
	struct v4l1_device *dev = (struct v4l1_device *)file->device_info;

	int ret = 0;
	switch(cmd) {
	case VIDIOCGCAP:
	{
		struct video_capability *cap = arg;
		struct v4l2_capability  v4l2cap;

		memset(cap, 0, sizeof(*cap));
		ret = ioctl(dev->v4l2_fd, VIDIOC_QUERYCAP, &v4l2cap);
		if (ret < 0)
			return -errno;
		memcpy(cap->name, v4l2cap.card,
		       MIN(sizeof(cap->name), sizeof(v4l2cap.card)));
		cap->name[sizeof(cap->name) - 1] = 0;
		if (v4l2cap.capabilities & V4L2_CAP_VIDEO_CAPTURE)
			cap->type |= VID_TYPE_CAPTURE;
		if (v4l2cap.capabilities & V4L2_CAP_TUNER)
			cap->type |= VID_TYPE_TUNER;
		if (v4l2cap.capabilities & V4L2_CAP_VBI_CAPTURE)
			cap->type |= VID_TYPE_TELETEXT;
		if (v4l2cap.capabilities & V4L2_CAP_VIDEO_OVERLAY)
			cap->type |= VID_TYPE_OVERLAY;
		cap->channels = count_inputs(dev->v4l2_fd);
		cap->audios = 0;
		cap->minheight = 0;
		cap->minwidth = 0;
		cap->maxheight = 480;
		cap->maxwidth = 640;
		check_size(dev->v4l2_fd, &cap->maxwidth, &cap->maxheight);
		check_size(dev->v4l2_fd, &cap->minwidth, &cap->minheight);
		break;
	}
	case VIDIOCGWIN:
	{
		struct video_window *vw = arg;
		struct v4l2_format v4l2fmt;

		v4l2fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		ret = ioctl(dev->v4l2_fd, VIDIOC_G_FMT, &v4l2fmt);
		if (ret < 0)
			return -errno;
		vw->x = 0;
		vw->y = 0;
		vw->width = v4l2fmt.fmt.pix.width;
		vw->height = v4l2fmt.fmt.pix.height;
		vw->chromakey = 0;
		vw->clips = NULL;
		vw->flags = 0;
		vw->clipcount = 0;
		break;
	}
	case VIDIOCGPICT:
	{
		struct video_picture *pict = arg;
		struct v4l2_format v4l2fmt;
		pict->brightness = get_v4l_control(dev->v4l2_fd, V4L2_CID_BRIGHTNESS);
		pict->hue = get_v4l_control(dev->v4l2_fd, V4L2_CID_HUE);
		pict->contrast = get_v4l_control(dev->v4l2_fd, V4L2_CID_CONTRAST);
		pict->colour = get_v4l_control(dev->v4l2_fd, V4L2_CID_SATURATION);
		pict->whiteness = get_v4l_control(dev->v4l2_fd, V4L2_CID_WHITENESS);

		v4l2fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		ret = ioctl(dev->v4l2_fd, VIDIOC_G_FMT, &v4l2fmt);
		if (ret < 0)
			return -errno;

		pict->depth = ((v4l2fmt.fmt.pix.bytesperline << 3) +
			(v4l2fmt.fmt.pix.width - 1)) / v4l2fmt.fmt.pix.width;
		pict->palette = pixelformat_to_palette(v4l2fmt.fmt.pix.pixelformat);
		break;
	}
	case VIDIOCSWIN:
	{
		struct video_window *vw = arg;
		struct v4l2_format v4l2fmt;

		v4l2_stream_off(dev);
		v4l2_free_buffers(dev);
		v4l2fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		ret = ioctl(dev->v4l2_fd, VIDIOC_G_FMT, &v4l2fmt);
		if(ret < 0) {
			v4l2_request_buffers(dev, NUM_BUFFERS);
			v4l2_stream_on(dev);
			return -errno;
		}
		v4l2fmt.fmt.pix.width  = vw->width > 640 ? 640 : vw->width;
		v4l2fmt.fmt.pix.height = vw->height > 480 ? 480 : vw->height;
		v4l2fmt.fmt.pix.field  = V4L2_FIELD_ANY;
		v4l2fmt.fmt.pix.bytesperline = 0;
		ret = ioctl(dev->v4l2_fd, VIDIOC_S_FMT, &v4l2fmt);
		if (ret < 0) {
			v4l2_request_buffers(dev, NUM_BUFFERS);
			v4l2_stream_on(dev);
			return -errno;
		}
		vw->width  = v4l2fmt.fmt.pix.width;
		vw->height = v4l2fmt.fmt.pix.height;
		v4l2_request_buffers(dev, NUM_BUFFERS);
		v4l2_stream_on(dev);
		break;
	}
	case VIDIOCSPICT:
	{
		struct video_picture *pict = arg;
		struct v4l2_format v4l2fmt;


		v4l2fmt.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		ret = ioctl(dev->v4l2_fd, VIDIOC_G_FMT, &v4l2fmt);
		if (ret < 0)
			return -errno;
		if (v4l2fmt.fmt.pix.pixelformat != palette_to_pixelformat(pict->palette)) {
			v4l2_stream_off(dev);
			v4l2_free_buffers(dev);
			v4l2fmt.fmt.pix.pixelformat = palette_to_pixelformat(pict->palette);
			ret = ioctl(dev->v4l2_fd, VIDIOC_S_FMT, &v4l2fmt);
			if (ret < 0) {
				ret = -errno;
			}
			v4l2_request_buffers(dev, NUM_BUFFERS);
			v4l2_stream_on(dev);
		}
		if (ret == 0) {
			set_v4l_control(dev->v4l2_fd, V4L2_CID_BRIGHTNESS, pict->brightness);
			set_v4l_control(dev->v4l2_fd, V4L2_CID_HUE, pict->hue);
			set_v4l_control(dev->v4l2_fd, V4L2_CID_CONTRAST, pict->contrast);
			set_v4l_control(dev->v4l2_fd, V4L2_CID_SATURATION, pict->colour);
			set_v4l_control(dev->v4l2_fd, V4L2_CID_WHITENESS, pict->whiteness);
		}
		break;
	}
	case VIDIOCGCHAN:
	{
		struct video_channel *chan = arg;
		struct v4l2_input input;
		v4l2_std_id std;
		input.index = chan->channel;
		ret = ioctl(dev->v4l2_fd, VIDIOC_ENUMINPUT, &input);
		if (ret < 0)
			return -errno;
		memcpy(chan->name, input.name,
		       MIN(sizeof(chan->name), sizeof(input.name)));
		chan->tuners = (input.type == V4L2_INPUT_TYPE_TUNER ? 1 : 0);
		chan->flags = (chan->tuners ? VIDEO_VC_TUNER : 0);
		switch(input.type) {
		case V4L2_INPUT_TYPE_TUNER:
			chan->type = VIDEO_TYPE_TV;
			break;
		case V4L2_INPUT_TYPE_CAMERA:
			chan->type = VIDEO_TYPE_CAMERA;
			break;
		}
		chan->norm = 0;
		ret = ioctl(dev->v4l2_fd, VIDIOC_G_STD, &std);
		if (ret == 0) {
			if (std & V4L2_STD_PAL)
				chan->norm = VIDEO_MODE_PAL;
			if (std & V4L2_STD_NTSC)
				chan->norm = VIDEO_MODE_NTSC;
			if (std & V4L2_STD_SECAM)
				chan->norm = VIDEO_MODE_SECAM;
		}
		break;
	}
	case VIDIOCSCHAN:
	{
		struct video_channel *chan = arg;
		v4l2_std_id std = 0;
		v4l2_stream_off(dev);
		v4l2_free_buffers(dev);
		ret = ioctl(dev->v4l2_fd, VIDIOC_S_INPUT, &chan->channel);
		if (ret < 0) {
			v4l2_request_buffers(dev, NUM_BUFFERS);
			v4l2_stream_on(dev);
			return -errno;
		}
		switch (chan->norm) {
		case VIDEO_MODE_PAL:
			std = V4L2_STD_PAL;
			break;
		case VIDEO_MODE_NTSC:
			std = V4L2_STD_NTSC;
			break;
		case VIDEO_MODE_SECAM:
			std = V4L2_STD_SECAM;
			break;
		}
		if (0 != std)
			ioctl(dev->v4l2_fd, VIDIOC_S_STD, &std);
		v4l2_request_buffers(dev, NUM_BUFFERS);
		v4l2_stream_on(dev);
		break;
	}
	case VIDIOCGMBUF:
	{
		struct video_mbuf *mbuf = arg;
		int i;

		memset(mbuf, 0, sizeof(struct video_mbuf));
		mbuf->frames = NUM_BUFFERS;
		mbuf->size = dev->v4l1buffers.size;
		for(i = 0; i < NUM_BUFFERS; i++)
			mbuf->offsets[i] = dev->v4l1buffers.offsets[i];
		break;
	}
	case VIDIOCMCAPTURE:
	{
		struct video_mmap *mmap = arg;
		break;
	}
	case VIDIOCSYNC:
	{
		int *framenum = arg;
		struct v4l2_buffer buf;
		buf.type =  V4L2_BUF_TYPE_VIDEO_CAPTURE;
		buf.memory = V4L2_MEMORY_MMAP;
		ret = ioctl(dev->v4l2_fd, VIDIOC_DQBUF, &buf);
		if (ret < 0)
			return -errno;
		memcpy(dev->v4l1buffers.mem + dev->v4l1buffers.offsets[*framenum], dev->v4l2buffers[buf.index].start, buf.bytesused);
		ioctl(dev->v4l2_fd, VIDIOC_QBUF, &buf);
		break;
	}
	default:
	{
		ret = -EINVAL;
	}
	}

	return ret;
}

int v4l1_mmap(struct fusd_file_info *file, int offset, size_t length, int flags, void **addr, size_t *out_length)
{
	int i;
	struct v4l1_device *dev = file->device_info;
	*addr = dev->v4l1buffers.mem;
	*out_length = dev->v4l1buffers.size;
	return 0;
}

int v4l1_read(struct fusd_file_info *file, char *buffer, size_t size, loff_t *offset)
{
	struct v4l2_buffer buf;
	buf.type =  V4L2_BUF_TYPE_VIDEO_CAPTURE;
	buf.memory = V4L2_MEMORY_MMAP;
	size_t count;
	int ret;
	struct v4l1_device *dev = file->device_info;
	if (dev->read_buffer == NULL) {
		ret = ioctl(dev->v4l2_fd, VIDIOC_DQBUF, &buf);
		if (ret < 0)
			return ret;
		memcpy(&(dev->v4l2buffers[buf.index].buf), &buf, sizeof(struct v4l2_buffer));
		dev->read_buffer = &dev->v4l2buffers[buf.index];
	}
	count = MIN((size_t)(dev->read_buffer->buf.bytesused - *offset), size);
	memcpy(buffer, dev->read_buffer->start + *offset, count);
	*offset += count;
	if (*offset >= dev->read_buffer->buf.bytesused) {
		buf.index = dev->read_buffer->buf.index;
		ret = ioctl(dev->v4l2_fd, VIDIOC_QBUF, &buf);
		dev->read_buffer = NULL;
		*offset = 0;
	}
	return count;
}

int v4l1_alloc_buffer(struct v4l1_buffers *buffers, int count)
{
	int flags, i;
	int ret = 0;
	buffers->size = count * V4L1_BUFFER_SIZE;
	buffers->mem = mmap(NULL, buffers->size, PROT_READ | PROT_WRITE, MAP_SHARED | MAP_ANONYMOUS, 0, 0);
	if (buffers->mem == (void *)-1) {
		return -ENOMEM;
	}
	for (i = 0; i < count; i++) {
		buffers->offsets[i] = i * V4L1_BUFFER_SIZE;
	}
	return 0;
}

int v4l1_free_buffer(struct v4l1_buffers *buffers)
{
	int flags, i;
	munmap(buffers->mem, buffers->size);
	for (i = 0; i < NUM_BUFFERS; i++) {
		buffers->offsets[i] = 0;
	}
	return 0;
}

int v4l2_stream_on(struct v4l1_device *dev)
{
	int stream_type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	ioctl(dev->v4l2_fd, VIDIOC_STREAMON, &stream_type);
}

int v4l2_stream_off(struct v4l1_device *dev)
{
	int stream_type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	ioctl(dev->v4l2_fd, VIDIOC_STREAMOFF, &stream_type);
}

int v4l2_request_buffers(struct v4l1_device *dev, int count)
{
	struct v4l2_requestbuffers request;
	struct v4l2_buffer *buf;
	int ret = 0;
	int i;

	request.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	request.memory = V4L2_MEMORY_MMAP;
	request.count = count;
	ret = ioctl(dev->v4l2_fd, VIDIOC_REQBUFS, &request);
	if (ret < 0)
		return -errno;
	if (request.count > count)
		return -EINVAL;
	dev->read_buffer = NULL;
	for (i = 0; i < request.count; i++) {
		buf = &dev->v4l2buffers[i].buf;
		buf->type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
		buf->memory = V4L2_MEMORY_MMAP;
		buf->index = i;
		ret = ioctl(dev->v4l2_fd, VIDIOC_QUERYBUF, buf);
		if (ret == 0) {
			dev->v4l2buffers[i].start = mmap(NULL, buf->length, PROT_READ | PROT_WRITE, MAP_SHARED, dev->v4l2_fd, buf->m.offset);
			if(dev->v4l2buffers[i].start != (void *)-1) {
				info("mmaping buffer %d to %lX\n", i, (unsigned long)dev->v4l2buffers[i].start);
				ret = ioctl(dev->v4l2_fd, VIDIOC_QBUF, buf);
			}
		}
	}
	return 0;
}

int v4l2_free_buffers(struct v4l1_device *dev)
{
	int i;
	struct v4l2_requestbuffers request;
	for (i = 0; i < NUM_BUFFERS; i++) {
		if (dev->v4l2buffers[i].start != (void *)-1 && dev->v4l2buffers[i].start != NULL) {
			munmap(dev->v4l2buffers[i].start, dev->v4l2buffers[i].buf.length);
			info("Munmapping buffer %d at %lX\n", i, (unsigned long)dev->v4l2buffers[i].start);
		}
	}

	request.count = 0;
	request.type = V4L2_BUF_TYPE_VIDEO_CAPTURE;
	request.memory = V4L2_MEMORY_MMAP;
	ioctl(dev->v4l2_fd, VIDIOC_REQBUFS, &request);
	return 0;
}

int v4l2_open(char* filename) {
	int fd;
	struct v4l2_capability  v4l2cap;
	fd = open(filename, O_RDWR);
	if (fd < 0) {
		info("Device %s not found.\n", filename);
		return -1;
	}
	if (ioctl(fd, VIDIOC_QUERYCAP, &v4l2cap) < 0) {
		close(fd);
		info("Device %s is not v4l2 compliant. Error (%d)\n", filename, errno);
		return -1;
	}
	if (!(v4l2cap.capabilities & V4L2_CAP_VIDEO_CAPTURE) ||
            !(v4l2cap.capabilities & V4L2_CAP_STREAMING)) {
		close(fd);
		info("Device %s is not a capture device\n", filename);
		return -1;
	}
	return fd;
}

void parse_arguments(int argc, char **argv)
{
	struct poptOption options[] = {
		{"input", 'i', POPT_ARG_STRING, &v4l2_dev, 0,"V4l2 input device (default: /dev/video0)", "device"},
		{"output", 'o', POPT_ARG_STRING, &v4l1_dev, 0,"V4l1 output device (default: /dev/video1)", "device"},
		POPT_AUTOHELP
		{ NULL, 0, 0, NULL, 0 }
	};
	poptContext ctx = poptGetContext(NULL, argc, (const char**)argv, options , POPT_CONTEXT_POSIXMEHARDER);
	poptGetNextOpt(ctx);
	poptFreeContext(ctx);
}

int main(int argc, char *argv[])
{
	int i, status;
	fd_set fuse_fds;
	sigset_t sigs;
	struct v4l1_device *dev;
	struct timespec timeout = {0, 0};

	parse_arguments(argc, argv);

	mlockall(MCL_CURRENT | MCL_FUTURE);
	sigemptyset( &sigs );
	sigaddset( &sigs, SIGINT );
	sigaddset( &sigs, SIGTERM );

	sigprocmask( SIG_BLOCK, &sigs, NULL);

	info("V4l2 -> v4l1 Capture device\n");
	dev = malloc(sizeof(struct v4l1_device));
	if (dev == NULL) {
		info("Out of Memory\n");
		return -1;
	}
	dev->v4l2_fd = v4l2_open(v4l2_dev);
	if (dev->v4l2_fd < 0) {
		free(dev);
		return -1;
	}
	close(dev->v4l2_fd);
	dev->v4l1_fd = fusd_register(v4l1_dev, "video4linux", v4l1_dev+5, 0666, dev, &v4l1_file_ops);
	if (dev->v4l1_fd < 0) {
		info("Failed to register fusd device.\n");
		free(dev);
		return -1;
	}
	info("v4l2:%s -> v4l1:%s\n", v4l2_dev, v4l1_dev);
	while (1) {
		FD_ZERO(&fuse_fds);
		FD_SET(dev->v4l1_fd, &fuse_fds);
		status = sigtimedwait(&sigs, NULL, &timeout);
		if (status > 0) {
			break;
		}
		status = pselect(dev->v4l1_fd + 1, &fuse_fds, NULL, NULL, &timeout, NULL);
		if (status <= 0) {
			if (status < 0)
				perror("libfusd: fusd_run: error on select");
			continue;
		}
		if (FD_ISSET(dev->v4l1_fd, &fuse_fds))
			fusd_dispatch(dev->v4l1_fd);
	}
	fusd_unregister(dev->v4l1_fd);
	free(dev);
	return 0;
}

