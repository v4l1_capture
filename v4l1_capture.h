/*
    Copyright (C) 2008 Brian Johnson

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
*/

#ifndef _V4L1_CAPTURE_H
#define _V4L1_CAPTURE_H

#define ARRAY_SIZE(arr) (sizeof(arr) / sizeof((arr)[0]))
#define PAGE_ALIGN(len) ((((len) / 4096) + 1) * 4096)

#define MIN(a, b)	((a) < (b) ? (a) : (b))

#define V4L1_BUFFER_SIZE	PAGE_ALIGN(640 * 480 * 4)

#define NUM_BUFFERS		5

#define info(format, arg...) syslog(LOG_INFO, "" format "", ## arg)


struct v4l1_buffers {
	int offsets[NUM_BUFFERS];
	char* mem;
	size_t size;
};

struct v4l2_buffers {
	struct v4l2_buffer buf;
	void *start;
};

struct v4l1_device {
	int v4l1_fd;
	int v4l2_fd;

	struct v4l1_buffers v4l1buffers;
	struct v4l2_buffers v4l2buffers[NUM_BUFFERS];
	struct v4l2_buffers *read_buffer;
};

int v4l1_open(struct fusd_file_info *);
int v4l1_close(struct fusd_file_info *);
int v4l1_ioctl(struct fusd_file_info *, int, void *);
int v4l1_read(struct fusd_file_info *, char *, size_t, loff_t *);
int v4l1_mmap(struct fusd_file_info *, int, size_t, int, void **, size_t *);

int v4l2_request_buffers(struct v4l1_device *, int);
int v4l2_free_buffers(struct v4l1_device *);

#endif
