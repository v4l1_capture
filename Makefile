PREFIX=/usr/local/
CFLAGS=-pg -g
LDFLAGS=-lfusd -lpthread -lpopt
CC=gcc

all: v4l1_capture

v4l1_capture: v4l1_capture.c
	$(CC) $(CFLAGS) $(LDFLAGS) -o $@ v4l1_capture.c

install: all
	install -d $(PREFIX)
	install -d $(PREFIX)/bin
	install -m 755 v4l1_capture $(PREFIX)/bin

uninstall:
	rm -f $(PREFIX)/bin/v4l1_capture

clean:
	rm -f v4l1_capture
	rm -f *.o
